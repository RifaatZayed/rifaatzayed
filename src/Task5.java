public class Task5 {
    public static Object[] functions(int[] a) {

        int min=0;
        int max=0;
        int sum=0;
        double avg=0.0;

        for (int i = 0; i < a.length; i++) {

            sum+=a[i];
            int j=i+1;

            if(a[i] > a[max])
                max = i ;

            if(a[i] < a[min])
                min = i ;

        }
        avg=(double)sum/(a.length);

        Object[] array={sum,avg,a[min],a[max]};
        return array;
    }
    public static void main(String[] args) {
        int[]arr={5,9,200,22,11,44,7,12,3};
        functions(arr);
        Object[]x=functions(arr);
        for(int i=0;i<x.length;i++){
            System.out.println(x[i]);
        }


    }
}
