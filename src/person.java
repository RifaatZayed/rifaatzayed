import java.util.Objects;

public class person {

    public static class Person{
         public  String name ;
       public int ID;
       public int age;

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", ID=" + ID +
                    ", age=" + age +
                    '}';
        }

       /* @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Person person = (Person) o;
            return ID == person.ID &&
                    age == person.age &&
                    Objects.equals(name, person.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, ID, age);
        }*/
       @Override
       public boolean equals(Object obj) {
           if(obj instanceof  Person)
           {
               Person t = (Person) obj ;
               return name == t.name && ID == t.ID && age == t.age ;
           }
           return  false ;
       }
    }
    public static void main(String[] args) {
        Person p1=new Person();
        Person p2 =new Person();
        p1.name="rifaat";
        p1.age=25;
        p1.ID=201610353;
        p2.name="rifaat";
        p2.age=25;
        p2.ID=201610353;
        System.out.println(p1);
        System.out.println(p1.equals(p2));


    }

}

