

import java.util.Arrays;


public class AlienPack {
    private Alien[] aliens;

    public AlienPack(int numAliens) {
        aliens = new Alien[numAliens];
    }

    public void addAlien(Alien newAlien, int index) {
        aliens[index] = newAlien;
    }

    @Override
    public String toString() {
        return "AlienPack{" +
                "aliens=" + Arrays.toString(aliens) +
                '}';
    }

    public Alien[] getAliens() {

        return aliens;
    }

    public int calculateDamage() {
        Alien.GetDamage();


        return Alien.damage;
    }

    public static void main(String[] args) {
        Alien.Snake s1=new Alien.Snake(50,"dog");
        Alien.Snake s2=new Alien.Snake(50,"dog");
        Alien.Orge o1  =new Alien.Orge(27,"CAT");
        Alien.Marshmillo m1 =new Alien.Marshmillo(100,"monkey");
        Alien.Marshmillo m2  =new Alien.Marshmillo(100,"cow");
        AlienPack a =new AlienPack(5);
        a.addAlien(s1,0);
        a.addAlien(o1,1);
        a.addAlien(m1,2);
        a.addAlien(m2,3);
        a.addAlien(s2,4);
        s1.GetDamage();
        m2.GetDamage();
        System.out.println(  a.toString());
        System.out.println(a.calculateDamage());


    }
}
